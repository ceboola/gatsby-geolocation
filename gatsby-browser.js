import Layout from '@components/Layout';
import SearchProvider from '@providers/SearchProvider';
import UserProvider from '@providers/UserProvider';

export const wrapPageElement = ({ element }) => {
  return (
    <SearchProvider>
      <UserProvider>
        <Layout>{element}</Layout>
      </UserProvider>
    </SearchProvider>
  );
};
