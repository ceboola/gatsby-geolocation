module.exports = {
  siteMetadata: {
    title: `gatsby-geolocation`,
    description: `gatsby-geolocation`,
    author: `@gatsbyjs`,
    siteUrl: `https://blog.codesigh.com/`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-geolocation`,
        short_name: `geolocation`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-tsconfig-paths`,
    `gatsby-plugin-styled-components`,
    'gatsby-plugin-antd',
    'gatsby-plugin-react-leaflet',
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
};
