import { createContext, useState, FC } from 'react';

interface IContext {
  apiFetched: boolean;
  setApiFetched: (state: boolean) => void;
  userData: GlobalIpStack;
  setUserData: (obj: GlobalIpStack) => void;
}
export const Context = createContext<IContext>({
  apiFetched: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setApiFetched: () => {},
  userData: {
    ip: '',
    type: '',
    continent_name: '',
    country_name: '',
    city: '',
    zip: '',
    location: {
      capital: '',
      country_flag: '',
    },
    longitude: 0,
    latitude: 0,
  },
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setUserData: () => {},
});
const UserProvider: FC = ({ children }) => {
  const [userData, setUserData] = useState<GlobalIpStack>({} as GlobalIpStack);
  const [apiFetched, setApiFetched] = useState(false);

  return (
    <Context.Provider
      value={{ userData, setUserData, apiFetched, setApiFetched }}
    >
      {children}
    </Context.Provider>
  );
};

export default UserProvider;
