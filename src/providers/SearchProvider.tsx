import { createContext, useState, FC } from 'react';

interface IContext {
  apiFetched: boolean;
  setApiFetched: (state: boolean) => void;
  data: GlobalIpStack;
  setData: (obj: GlobalIpStack) => void;
  searches: string[];
  addSearch: (greeting: string) => void;
}
export const Context = createContext<IContext>({
  apiFetched: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setApiFetched: () => {},
  data: {
    ip: '',
    type: '',
    continent_name: '',
    country_name: '',
    city: '',
    zip: '',
    location: {
      capital: '',
      country_flag: '',
    },
    longitude: 0,
    latitude: 0,
  },
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setData: () => {},
  searches: [],
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  addSearch: () => {},
});
const SearchProvider: FC = ({ children }) => {
  const [searches, setSearches] = useState<IContext['searches']>([]);
  const [data, setData] = useState<GlobalIpStack>({} as GlobalIpStack);
  const [apiFetched, setApiFetched] = useState(false);
  const addSearch = (search: string) => {
    setSearches((searches) => [search, ...searches]);
  };

  return (
    <Context.Provider
      value={{ searches, addSearch, data, setData, apiFetched, setApiFetched }}
    >
      {children}
    </Context.Provider>
  );
};

export default SearchProvider;
