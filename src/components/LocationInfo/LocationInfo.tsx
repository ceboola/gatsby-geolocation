import { VFC } from 'react';
import { Divider, List, Row, Col, Typography } from 'antd';

import Empty from '@components/Empty';

import { Wrapper, FlagImage } from './styled';
import { LocationInfoProps } from './types';

const LocationInfo: VFC<LocationInfoProps> = ({ data }) => {
  return (
    <Wrapper>
      {data ? (
        <>
          <Row gutter={[16, 32]}>
            <Col span={12}>
              <Divider>User</Divider>
              <List size="small" bordered>
                <List.Item>
                  <Typography.Text mark>IP</Typography.Text>
                  {data.ip}
                </List.Item>
                <List.Item>
                  <Typography.Text mark>TYPE</Typography.Text>
                  {data.type}
                </List.Item>
                <List.Item>
                  <Typography.Text mark>CONTINENT</Typography.Text>
                  {data.continent_name}
                </List.Item>
                <List.Item>
                  <Typography.Text mark>COUNTRY</Typography.Text>
                  {data.country_name}
                </List.Item>
              </List>
            </Col>
            <Col span={12}>
              <Divider>Location</Divider>
              <List size="small" bordered>
                <List.Item>
                  <Typography.Text mark>CITY</Typography.Text>
                  {data.city}
                </List.Item>
                <List.Item>
                  <Typography.Text mark>ZIP-CODE</Typography.Text>
                  {data.zip}
                </List.Item>
                <List.Item>
                  <Typography.Text mark>LATITUDE</Typography.Text>
                  {data.latitude.toFixed(4)}
                </List.Item>
                <List.Item>
                  <Typography.Text mark>LONGITUDE</Typography.Text>
                  {data.longitude.toFixed(4)}
                </List.Item>
              </List>
            </Col>
          </Row>
          <FlagImage src={data.location.country_flag} />
        </>
      ) : (
        <Empty description="No data, please fill search above" />
      )}
    </Wrapper>
  );
};

export default LocationInfo;
