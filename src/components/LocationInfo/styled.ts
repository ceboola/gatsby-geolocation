import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 300px;
  border: 1px solid green;
  position: relative;
`;

export const FlagImage = styled.img`
  width: auto;
  height: 40px;
  position: absolute;
  bottom: 0;
  right: 0;
`;
