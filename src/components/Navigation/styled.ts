import styled from 'styled-components';

import { Menu } from 'antd';

export const Title = styled.h1`
  display: flex;
  align-items: center;
  font-size: 18px;
  margin: 0;
  color: #ff5b5b;
  font-weight: 700;
`;

export const MenuAntd = styled(Menu)`
  .ant-menu-item-selected {
    span {
      color: #fff;
    }
  }
  margin-left: auto;
  font-size: 15px;
  font-weight: 700;
  span {
    color: #ff5b5b;
  }
  li {
    &:hover {
      span {
        color: #fff;
      }
    }
  }
`;
