import { VFC, memo } from 'react';
import { Link } from 'gatsby';

import { Title, MenuAntd } from './styled';
import { NavigationProps } from './types';

const Navigation: VFC<NavigationProps> = ({ siteTitle }) => {
  return (
    <>
      <Title>{siteTitle}</Title>
      <MenuAntd theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
        <MenuAntd.Item key="1">
          <Link to="/">Home</Link>
        </MenuAntd.Item>
        <MenuAntd.Item key="2">
          <Link to="/Page2/">Location</Link>
        </MenuAntd.Item>
      </MenuAntd>
    </>
  );
};

export default memo(Navigation);
