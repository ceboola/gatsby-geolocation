import { memo, useContext, VFC } from 'react';
import { Typography } from 'antd';

import { Context } from '@src/providers/SearchProvider';
import Empty from '@components/Empty';

const SearchHistory: VFC = () => {
  const { Title, Text } = Typography;
  const { searches } = useContext(Context);
  return (
    <div>
      <Title level={3}>Search History</Title>
      {searches.length > 0 ? (
        searches.map((val) => (
          <div>
            <Text underline>{val}</Text>
          </div>
        ))
      ) : (
        <Empty description="no previous searches" />
      )}
    </div>
  );
};

export default memo(SearchHistory);
