import { VFC } from 'react';
import { StyledEmpty } from './styled';

const Empty: VFC<{ description: string }> = ({ description }) => {
  return <StyledEmpty description={description} />;
};

export default Empty;
