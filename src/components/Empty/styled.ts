import styled from 'styled-components';
import { Empty } from 'antd';

export const StyledEmpty = styled(Empty)`
  height: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`;
