import styled from 'styled-components';
import { Layout } from 'antd';

const { Header: HeaderAntd, Content: ContentAntd, Footer: FooterAntd } = Layout;

export const Header = styled(HeaderAntd)`
  display: flex;
`;

export const Content = styled(ContentAntd)`
  padding: 24px 50px;
  display: flex;
  flex-direction: column;
  align-items: center;
  height: auto;
`;

export const Footer = styled(FooterAntd)`
  display: flex;
  align-items: center;
  flex-direction: column;
`;
