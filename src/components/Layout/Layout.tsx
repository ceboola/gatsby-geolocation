import { FC, memo } from 'react';
import { useStaticQuery, graphql } from 'gatsby';

import Navigation from '@src/components/Navigation';
import { Layout as LayoutAntd, Affix } from 'antd';

import { Header, Content, Footer } from './styled';

const Layout: FC = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  return (
    <>
      <LayoutAntd>
        <Affix style={{ zIndex: 9999 }} offsetTop={0}>
          <Header>
            <Navigation siteTitle={data.site.siteMetadata?.title || `Title`} />
          </Header>
        </Affix>
        <Content>{children}</Content>
        <Footer>
          © {new Date().getFullYear()}, Built with
          {` `}
          <a href="https://www.gatsbyjs.com">Gatsby</a>
        </Footer>
      </LayoutAntd>
    </>
  );
};

export default memo(Layout);
