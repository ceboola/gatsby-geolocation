import { LatLngExpression } from 'leaflet';

export interface LocationProps {
  location: LatLngExpression;
  zoomLevel: number;
}

export interface ChangeViewProps {
  center: LatLngExpression;
  zoom: number;
}
