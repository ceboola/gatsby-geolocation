import { VFC } from 'react';
import { MapContainer, TileLayer, Marker, Popup, useMap } from 'react-leaflet';

import Empty from '@components/Empty';

import { Wrapper } from './styled';
import { LocationProps, ChangeViewProps } from './types';

const Location: VFC<LocationProps> = ({ location, zoomLevel }) => {
  const ChangeView = ({ center, zoom }: ChangeViewProps) => {
    const map = useMap();
    map.setView(center, zoom);
    return null;
  };
  return (
    <Wrapper>
      {location ? (
        <MapContainer
          style={{ height: '100%' }}
          center={location}
          zoom={zoomLevel}
          scrollWheelZoom={false}
        >
          <ChangeView center={location} zoom={zoomLevel} />
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={location}>
            <Popup>you're here</Popup>
          </Marker>
        </MapContainer>
      ) : (
        <Empty description="No data, please fill search above" />
      )}
    </Wrapper>
  );
};

export default Location;
