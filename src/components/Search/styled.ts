import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 24px 0;
`;

export const Info = styled.div<{ isValid: boolean }>`
  background-color: ${(props) => (props.isValid ? 'green' : 'red')};
  font-weight: 700;
  display: flex;
  justify-content: center;
`;
