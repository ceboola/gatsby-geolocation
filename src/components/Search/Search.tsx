import { useContext, useState, VFC } from 'react';
import { Input, Space } from 'antd';

import { Context } from '@src/providers/SearchProvider';

import { Info, Wrapper } from './styled';

const { Search: SearchAntd } = Input;

const Search: VFC = () => {
  const { addSearch, setApiFetched } = useContext(Context);
  const [valid, setValid] = useState(false);
  const [text, setText] = useState('');
  const ipRegex =
    /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
  const urlRegex =
    /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)/;
  const onSearch = (value: string) => {
    if (valid) {
      addSearch(value);
      setApiFetched(false);
    }
  };

  const ValidateInput = (value: string) => {
    if (ipRegex.test(value) || urlRegex.test(value)) {
      setText('IP/URL matched, now you can query');
      setValid(true);
    } else {
      setText('Please provide in correct form IP/URL');
      setValid(false);
    }
    if (value.length === 0) setText('');
  };

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    ValidateInput(event.target.value);
  };
  return (
    <Wrapper>
      <Space direction="vertical">
        <SearchAntd
          placeholder="input search text"
          onSearch={onSearch}
          onChange={onChange}
          enterButton
        />
        <Info isValid={valid}>{text}</Info>
      </Space>
    </Wrapper>
  );
};

export default Search;
