import { VFC, memo } from 'react';
import { Row, Col } from 'antd';

import Search from '@components/Search';

const SearchSection: VFC = () => {
  return (
    <div>
      <Row gutter={[16, 32]}>
        <Col span={24}>
          <Search />
        </Col>
      </Row>
    </div>
  );
};

export default memo(SearchSection);
