import { useContext, VFC, memo, useEffect } from 'react';
import { Row, Col } from 'antd';
import axios from 'axios';

import Location from '@components/Location';
import LocationInfo from '@components/LocationInfo';
import { Context } from '@src/providers/SearchProvider';

const SearchSection: VFC = () => {
  const { searches, data, setData, apiFetched, setApiFetched } =
    useContext(Context);

  useEffect(() => {
    if (searches.length === 0) return;
    if (!apiFetched) {
      (async () => {
        try {
          const response = await axios.get(
            `http://api.ipstack.com/${searches[0]}?access_key=be092d2c5904d4c852092529d59c307b`,
          );
          setData(response.data);
          setApiFetched(true);
        } catch (err) {
          setData(err);
          setApiFetched(false);
        }
      })();
    }
  }, [searches, apiFetched]);

  return (
    <div>
      <Row gutter={[16, 32]}>
        <Col span={12}>
          <Location
            /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
            /* @ts-ignore */
            location={
              Object.keys(data).length > 0 && [data.latitude, data.longitude]
            }
            zoomLevel={17}
          />
        </Col>
        <Col span={12}>
          {/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */}
          {/* @ts-ignore */}
          <LocationInfo data={Object.keys(data).length > 0 && data} />
        </Col>
      </Row>
    </div>
  );
};

export default memo(SearchSection);
