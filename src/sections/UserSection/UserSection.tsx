import { useContext, useEffect, VFC } from 'react';
import { Row, Col, Spin } from 'antd';
import axios from 'axios';

import Location from '@components/Location';
import LocationInfo from '@components/LocationInfo';
import Empty from '@components/Empty';
import { Context } from '@src/providers/UserProvider';

const UserSection: VFC = () => {
  const { apiFetched, setApiFetched, userData, setUserData } =
    useContext(Context);
  useEffect(() => {
    if (apiFetched) return;
    (async () => {
      try {
        const response = await axios.get(
          `http://api.ipstack.com/check?access_key=be092d2c5904d4c852092529d59c307b`,
        );
        setUserData(response.data.content);
        setApiFetched(true);
      } catch (err) {
        setUserData(err);
        setApiFetched(false);
      }
    })();
  }, []);

  return (
    <div>
      {Object.keys(userData).length !== 0 ? (
        <Row gutter={[16, 32]}>
          <Col span={12}>
            <Location
              location={[
                Number(userData?.latitude),
                Number(userData?.longitude),
              ]}
              zoomLevel={17}
            />
          </Col>
          <Col span={12}>
            <LocationInfo data={userData} />
          </Col>
        </Row>
      ) : !apiFetched ? (
        <Spin size="large" />
      ) : (
        <Empty description="Sorry no information about this IP" />
      )}
    </div>
  );
};

export default UserSection;
