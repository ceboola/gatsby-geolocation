import Seo from '@components/Seo';

const Page2 = () => (
  <div style={{ height: '1000px' }}>
    <Seo title="Page2" />
    <h1>Page2</h1>
    <p>
      Just a test page that shows current session of search history is still
      alive
    </p>
  </div>
);

export default Page2;
