import Seo from '@components/Seo';
import SearchHistory from '@components/SearchHistory';
import SearchSection from '@sections/SearchSection';
import SearchResultsSection from '@sections/SearchResultsSection';
import UserSection from '@sections/UserSection';

const IndexPage = () => (
  <>
    <Seo title="Home" />
    <div style={{ display: 'flex', flexDirection: 'row', width: '100%' }}>
      <div style={{ flex: '0 1 20%' }}>
        <SearchHistory />
      </div>
      <div style={{ flex: '0 1 80%' }}>
        <UserSection />
        <SearchSection />
        <SearchResultsSection />
      </div>
    </div>
  </>
);

export default IndexPage;
